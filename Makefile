TOOLS_DIRS=acrylamid compass image_optimizer

images:
	@for dir in ${TOOLS_DIRS}; do \
		$(MAKE) -C $$dir image || exit 1; \
	done

push_images:
	@for dir in ${TOOLS_DIRS}; do \
		$(MAKE) -C $$dir push || exit 1; \
	done
